import { TestBed } from '@angular/core/testing';

import { ParserJSONService } from './parser-json.service';

describe('ParserJSONService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParserJSONService = TestBed.get(ParserJSONService);
    expect(service).toBeTruthy();
  });
});
