import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ParserJSONService {
    // myurl = '../assets/personsdb.json';
    myurl = 'http://timetable.sbmt.by/android/test/test3.json';

    constructor(public http: HttpClient) {
    }

    getData() {
        return this.http.get(this.myurl);
    }
}
