import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'stars'
})
export class StarsPipe implements PipeTransform {

    transform(mark: number): any {
        let stars: String;
        for (let i = 0; i < mark; i++) {
            stars += '<ion-icon [name]="star"></ion-icon>';
        }
        return '<div>Hello</div>';
        // return '<div [innerHTML]="stars"></div>';
    }

}
