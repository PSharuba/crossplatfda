import {NgModule} from '@angular/core';
import {StarsPipe} from './stars.pipe';

@NgModule({
    declarations: [StarsPipe],
    exports: [StarsPipe]
})
export class PipesModule {
}
