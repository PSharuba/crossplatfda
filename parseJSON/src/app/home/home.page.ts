import {Component, OnInit} from '@angular/core';
import {ParserJSONService} from '../parser-json.service';

export class Comment {
    rating: number;
    text: String;
    author: String;
}

export class Dishes {
    id: number;
    name: string;
    image: String;
    category: String;
    price: number;
    description: String;
    comment: Comment;
    label: String;
}

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    darray: Dishes[];


    constructor(public ms: ParserJSONService) {
    }


    ngOnInit(): void {
        // this.ms.getData().subscribe(data => this.parray = data['persones']);
        // this.ms.getData().subscribe(data => console.log("!!!",data));

        this.ms.getData().subscribe(data => {
            this.darray = data['dishes'];
            console.log('!!!', data);
        });
    }

}
