import {NgModule} from '@angular/core';
import {DaysLeftPipe} from './days-left.pipe';

@NgModule({
    declarations: [DaysLeftPipe],
    exports: [DaysLeftPipe]
})
export class PipesModule {
}
