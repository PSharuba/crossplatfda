import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityinfoPage } from './activityinfo.page';

describe('ActivityinfoPage', () => {
  let component: ActivityinfoPage;
  let fixture: ComponentFixture<ActivityinfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityinfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityinfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
