import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-activityinfo',
    templateUrl: './activityinfo.page.html',
    styleUrls: ['./activityinfo.page.scss'],
})
export class ActivityinfoPage implements OnInit {

    info: any;
    data: any;

    constructor(private activatedRoute: ActivatedRoute,
                private fileOpener: FileOpener,
                private storage: Storage,
                private router: Router) {
        this.activatedRoute.queryParams.subscribe((res) => {
            this.info = res;
            this.data = JSON.parse(res.data);
        });
    }

    ngOnInit() {
    }

    openFile(file: any) {
        const fileExtn = file.filePath.split('.').reverse()[0];
        const fileMIMEType = this.getMIMEtype(fileExtn);
        alert('ext: ' + fileExtn + ' mime: ' + fileMIMEType);
        this.fileOpener.open(file.filePath, fileMIMEType)
            .catch(e => alert('Error opening file:\n' + e));
    }

    getMIMEtype(extn) {
        const ext = extn.toLowerCase();
        const MIMETypes = {
            'txt': 'text/plain',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'pdf': 'application/pdf',
            'jpg': 'image/jpeg',
            'jpeg': 'image/jpeg',
            'bmp': 'image/bmp',
            'png': 'image/png',
            'xls': 'application/vnd.ms-excel',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'rtf': 'application/rtf',
            'ppt': 'application/vnd.ms-powerpoint',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        };
        return MIMETypes[ext];
    }

    returnFile(file: any) {
        let Files = [];
        this.storage.ready().then(() => {
                this.storage.get('files').then((val) => {
                    if (val != null) {
                        Files = val;
                    } else {
                        Files = [];
                    }
                });
            }
        );
        Files.push(
            {
                filePath: file.filePath,
                fileName: file.fileName,
                fileDescription: file.fileDescription
            }
        );
        this.storage.ready().then(() => {
                this.storage.set('files', Files);
            }
        );
    }

    /*redoProject(item: any) {
        const data = JSON.parse(item.data);
        const Project = {
            'title': data.title,
            'teacher': data.teacher,
            'date': data.date,
            'description': data.description
        };
        console.log(item);
        this.storage.ready().then(() => {
                this.storage.set('project', JSON.stringify(Project));
            }
        );
        this.router.navigate(
            ['/tabs/tab1/'],
            {queryParams: Project}
            );
    }*/
}
