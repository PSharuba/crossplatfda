import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})

export class Tab3Page implements OnInit {
    constructor(public navCtrl: NavController, private router: Router, private storage: Storage) {
    }

    lastActivity = [];

    ngOnInit(): void {
        this.storage.ready().then(() => {
                this.storage.get('files').then((val) => {
                    if (val != null) {
                        this.lastActivity = val;
                    } else {
                        this.lastActivity = [];
                    }
                });
            }
        );
    }

    viewAct(item: any) {
        this.router.navigate(
            ['/activityinfo'],
            {
                queryParams: item
            });
    }
}
