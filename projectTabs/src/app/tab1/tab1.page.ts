import {Component} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {OnInit} from '@angular/core';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
    constructor(public navCtrl: NavController, private router: Router, private storage: Storage) {
    }

    Project: any;

    ngOnInit(): void {
        this.storage.ready().then(() => {
                this.storage.get('project').then((val) => {
                    if (val != null) {
                        this.Project = JSON.parse(val);
                    } else {
                        this.Project = {
                            'title': 'New Project',
                            'teacher': 'NaN',
                            'date': undefined,
                            'description': 'NaN'
                        };
                    }
                });
                /*this.storage.get('lastActivity').then((val) => {
                    if (val != null) {
                        this.lastActivity = val;
                    } else {
                        this.lastActivity = [];
                    }
                });*/
            }
        );
    }

    clear() {
        this.Project = {
            'title': 'New Project',
            'teacher': 'NaN',
            'date': undefined,
            'description': 'NaN'
        };
        this.storage.ready().then(() => {
                this.storage.set('project', JSON.stringify(this.Project))
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
                this.storage.set('files', [])
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
                this.storage.set('lastActivity', [])
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
            }
        );

    }

    switchEdit() {
        const div = document.getElementById('editDetails');
        if (div.style.display === 'none') {
            div.style.display = 'block';
            // @ts-ignore
            document.getElementById('title').value = this.Project.title;
            // @ts-ignore
            document.getElementById('teacher').value = this.Project.teacher;
            // @ts-ignore
            document.getElementById('date').value = this.Project.date;
            // @ts-ignore
            document.getElementById('description').value = this.Project.description;
        } else {
            div.style.display = 'none';
        }
    }

    editProject() {
        // @ts-ignore
        let title = document.getElementById('title').value;
        // @ts-ignore
        let teacher = document.getElementById('teacher').value;
        // @ts-ignore
        let date = document.getElementById('date').value;
        // @ts-ignore
        let descr = document.getElementById('description').value;
        if (this.Project.date === undefined && (title === '' || teacher === '' || descr === '' || date === '')) {
            alert('Invalid input!');
            return;
        }
        if (title === '') {
            title = this.Project.title;
        }
        if (teacher === '') {
            teacher = this.Project.teacher;
        }
        if (descr === '') {
            descr = this.Project.description;
        }
        if (date === '') {
            date = this.Project.date;
        }
        const savedProject = this.Project;
        this.Project = {
            'title': title,
            'teacher': teacher,
            'date': date,
            'description': descr
        };
        let lastActivity = [];
        this.storage.ready().then(() => {
            this.storage.get('project').then((val) => {
                if (val != null) {
                    lastActivity = val;
                } else {
                    lastActivity = [];
                }
            });
        });
        const act = new Date();
        if (savedProject.date === undefined) {
            lastActivity.unshift(
                {
                    type: 'Project created',
                    name: 'Project ' + this.Project.title + ' created',
                    date: act.getDate().toString() + '-'
                        + (act.getMonth() + 1).toString() + '-'
                        + act.getFullYear().toString(),
                    dataType: 'project',
                    data: null
                }
            );
        } else {
            lastActivity.unshift(
                {
                    type: 'Project edited',
                    name: 'Project ' + this.Project.title + ' edited',
                    date: act.getDate().toString() + '-'
                        + (act.getMonth() + 1).toString() + '-'
                        + act.getFullYear().toString(),
                    dataType: 'project',
                    data: JSON.stringify(savedProject)
                }
            );
        }

        this.storage.ready().then(() => {
                this.storage.set('project', JSON.stringify(this.Project));
                this.storage.set('lastActivity', lastActivity);
            }
        );
        this.switchEdit();
    }
}
