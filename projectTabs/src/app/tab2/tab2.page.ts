import {Component} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {OnInit} from '@angular/core';
import {FileChooser} from '@ionic-native/file-chooser/ngx';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
    constructor(public navCtrl: NavController,
                private router: Router,
                private storage: Storage,
                private fileChooser: FileChooser,
                private fileOpener: FileOpener,
                private filePath: FilePath) {
    }

    Files = [];

    // lastActivity = [];


    ngOnInit(): void {
        this.storage.ready().then(() => {
                this.storage.get('files').then((val) => {
                    if (val != null) {
                        this.Files = val;
                    } else {
                        this.Files = [];
                    }
                });
                /*this.storage.get('lastActivity').then((val) => {
                    if (val != null) {
                        this.lastActivity = val;
                    } else {
                        this.lastActivity = [];
                    }
                });*/
            }
        );
    }

    chooseFile(FileName: string, FileDescription: string) {
        this.fileChooser.open().then(uri => {
            // get file path
            this.filePath.resolveNativePath(uri)
                .then(file => {
                    this.Files.push(
                        {
                            filePath: file,
                            fileName: FileName,
                            fileDescription: FileDescription
                        }
                    );
                })
                .catch(err => console.log(err));
        })
            .catch(e => {
                alert('Error: ' + JSON.stringify(e));
                this.Files.push(
                    {
                        filePath: '*path*',
                        fileName: FileName,
                        fileDescription: FileDescription
                    }
                );
            });
        let lastActivity = [];
        this.storage.ready().then(() => {
            this.storage.get('project').then((val) => {
                if (val != null) {
                    lastActivity = val;
                } else {
                    lastActivity = [];
                }
            });
        });
        const act = new Date();
        lastActivity.unshift(
            {
                type: 'File added',
                name: 'File ' + FileName + ' added',
                date: act.getDate().toString() + '-'
                    + (act.getMonth() + 1).toString() + '-'
                    + act.getFullYear().toString(),
                dataType: 'file',
                data: {
                    filePath: '*path*',
                    fileName: FileName,
                    fileDescription: FileDescription
                }
            }
        );
        this.storage.ready().then(() => {
                this.storage.set('files', this.Files);
                this.storage.set('lastActivity', lastActivity);
            }
        );
    }

    openFile(file: any) {
        const fileExtn = file.filePath.split('.').reverse()[0];
        const fileMIMEType = this.getMIMEtype(fileExtn);
        alert('ext: ' + fileExtn + ' mime: ' + fileMIMEType);
        this.fileOpener.open(file.filePath, fileMIMEType)
            .catch(e => alert('Error opening file:\n' + e));
    }

    getMIMEtype(extn) {
        const ext = extn.toLowerCase();
        const MIMETypes = {
            'txt': 'text/plain',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'pdf': 'application/pdf',
            'jpg': 'image/jpeg',
            'jpeg': 'image/jpeg',
            'bmp': 'image/bmp',
            'png': 'image/png',
            'xls': 'application/vnd.ms-excel',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'rtf': 'application/rtf',
            'ppt': 'application/vnd.ms-powerpoint',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        };
        return MIMETypes[ext];
    }

    addFile() {
        // @ts-ignore
        const name = document.getElementById('fileName').value;
        // @ts-ignore
        let descr = document.getElementById('fileDescr').value;
        if (name === '') {
            alert('No file name found!\nPlease enter file name');
            return;
        }
        if (descr === '') {
            descr = 'No description set.';
        }
        this.chooseFile(name, descr);
    }

    removeFile(file: any) {
        let lastActivity = [];
        this.storage.ready().then(() => {
            this.storage.get('project').then((val) => {
                if (val != null) {
                    lastActivity = val;
                } else {
                    lastActivity = [];
                }
            });
        });
        const act = new Date();
        lastActivity.unshift(
            {
                type: 'File removed',
                name: 'File ' + file.FileName + ' removed',
                date: act.getDate().toString() + '-'
                    + (act.getMonth() + 1).toString() + '-'
                    + act.getFullYear().toString(),
                dataType: 'file',
                data: file
            }
        );
        const index: number = this.Files.indexOf(file);
        this.Files.splice(index, 1);
        this.storage.ready().then(() => {
                this.storage.set('files', this.Files);
                this.storage.set('lastActivity', lastActivity);
            }
        );
    }
}
