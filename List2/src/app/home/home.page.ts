import {Component} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    items: any = [
        {drink: 'Coffee', icon: 'cafe', description: 'Good for breakfast'},
        {drink: 'Milk', icon: 'thumbs-up', description: 'Good for child'},
        {drink: 'Tea', icon: 'leaf', description: 'All day pleasure'},
        {drink: 'Beer', icon: 'beer', description: 'Evening only'}
    ];

    showInfo(item: any) {
        console.log(item.desciption);
    }

    constructor(public navCtrl: NavController) {

    }
}
