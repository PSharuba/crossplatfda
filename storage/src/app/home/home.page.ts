import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  person = {
    'name': 'John',
    'id': 1,
    'salary': 1000
  };
  rperson = {
    'name': '',
    'id': 0,
    'salary': 0
  };


  constructor(public storage: Storage) {
    storage.ready().then(() => {
          storage.set('person', JSON.stringify(this.person));
        }
    );
  }

  readMyStorage() {
    this.storage.get('person').then((val) => this.rperson = JSON.parse(val));
  }

}
