import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'daysLeft'
})
export class DaysLeftPipe implements PipeTransform {

    transform(projectDate: string): string {
        if (projectDate === undefined) {
            return 'No deadline set!';
        } else {
            const dateArr = projectDate.split('-');
            const date = new Date(parseFloat(dateArr[0]), parseFloat(dateArr[1]) - 1, parseFloat(dateArr[2]));
            const seconds = (date.valueOf() - new Date().valueOf()) / 1000;
            if (seconds <= 0) {
                return 'Outdated!';
            }
            const days = seconds / 86400;
            if (days >= 1) {
                return 'Days left: ' + Math.floor(days).toString();
            }
            return 'Hours left: ' + Math.floor((seconds - Math.floor(seconds / 86400)) / 3600).toString();
        }
    }

}
