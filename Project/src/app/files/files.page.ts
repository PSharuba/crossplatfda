import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {FileChooser} from '@ionic-native/file-chooser/ngx';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';
import {Events} from '@ionic/angular';

@Component({
    selector: 'app-files',
    templateUrl: './files.page.html',
    styleUrls: ['./files.page.scss'],
})
export class FilesPage implements OnInit {
    constructor(public navCtrl: NavController,
                private router: Router,
                private storage: Storage,
                private fileChooser: FileChooser,
                private fileOpener: FileOpener,
                private filePath: FilePath,
                private events: Events) {
        events.subscribe('upd', (time) => {
            console.log('Found updates. Updating data.\n',
                time.getDate() + '-' + time.getMonth() + '-' + time.getFullYear(), '\n' + time.getHours() + ':' + time.getMinutes());
            this.storage.ready().then(() => {
                    this.storage.get('files').then((val) => {
                        if (val != null) {
                            this.Files = val;
                        } else {
                            this.Files = [];
                        }
                    }).catch((e) => alert(e));
                }
            );
        });
    }

    Files = [];


    ngOnInit(): void {
        this.storage.ready().then(() => {
                this.storage.get('files').then((val) => {
                    if (val != null) {
                        this.Files = val;
                    } else {
                        this.Files = [];
                    }
                }).catch((e) => alert(e));
            }
        );
    }

    chooseFile(FileName: string, FileDescription: string) {
        this.fileChooser.open().then(uri => {
            // get file path
            this.filePath.resolveNativePath(uri)
                .then(file => {
                    this.Files.push(
                        {
                            filePath: file,
                            fileName: FileName,
                            fileDescription: FileDescription
                        }
                    );
                })
                .catch(err => console.log(err));
        })
            .catch(e => {
                alert('Error: ' + JSON.stringify(e));
                this.Files.push(
                    {
                        filePath: '*path*',
                        fileName: FileName,
                        fileDescription: FileDescription
                    }
                );
            });
        this.storage.ready().then(() => {
                this.storage.set('files', this.Files).catch((e) => alert(e));
                // this.storage.set('lastActivity', lastActivity);
            }
        );
    }

    openFile(file: any) {
        const fileExtn = file.filePath.split('.').reverse()[0];
        const fileMIMEType = this.getMIMEtype(fileExtn);
        alert('ext: ' + fileExtn + ' mime: ' + fileMIMEType);
        this.fileOpener.open(file.filePath, fileMIMEType)
            .catch(e => alert('Error opening file:\n' + e));
    }

    getMIMEtype(extn) {
        const ext = extn.toLowerCase();
        const MIMETypes = {
            'txt': 'text/plain',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'pdf': 'application/pdf',
            'jpg': 'image/jpg',
            'jpeg': 'image/jpeg',
            'bmp': 'image/bmp',
            'png': 'image/png',
            'xls': 'application/vnd.ms-excel',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'rtf': 'application/rtf',
            'ppt': 'application/vnd.ms-powerpoint',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        };
        return MIMETypes[ext];
    }

    addFile() {
        // @ts-ignore
        const name = document.getElementById('fileName').value;
        // @ts-ignore
        let descr = document.getElementById('fileDescr').value;
        if (name === '') {
            alert('No file name found!\nPlease enter file name');
            return;
        }
        if (descr === '') {
            descr = 'No description set.';
        }
        this.chooseFile(name, descr);
    }

    removeFile(file: any) {
        let removedFiles = [];
        this.storage.ready().then(() => {
            this.storage.get('removedFiles').then((val) => {
                if (val != null) {
                    removedFiles = val;
                    const act = new Date();
                    removedFiles.unshift(
                        {
                            date: act.getDate().toString() + '-'
                                + (act.getMonth() + 1).toString() + '-'
                                + act.getFullYear().toString(),
                            filePath: file.filePath,
                            fileName: file.fileName,
                            fileDescription: file.fileDescription
                        }
                    );
                    this.storage.ready().then(() => {
                        this.storage.set('removedFiles', removedFiles)
                            .then(() => this.events.publish('upd', new Date()))
                            .catch((e) => alert(e));
                    });
                } else {
                    removedFiles = [];
                    const act = new Date();
                    removedFiles.unshift(
                        {
                            date: act.getDate().toString() + '-'
                                + (act.getMonth() + 1).toString() + '-'
                                + act.getFullYear().toString(),
                            filePath: file.filePath,
                            fileName: file.fileName,
                            fileDescription: file.fileDescription
                        }
                    );
                    this.storage.ready().then(() => {
                        this.storage.set('removedFiles', removedFiles)
                            .then(() => this.events.publish('upd', new Date()))
                            .catch((e) => alert(e));
                    });
                }
            });
        });


        const index: number = this.Files.indexOf(file);
        this.Files.splice(index, 1);
        this.storage.ready().then(() => {
                this.storage.set('files', this.Files).then(() =>
                    this.events.publish('upd', new Date())).catch((e) => alert(e));
            }
        );

    }

    toHome() {
        this.router.navigate(['/']);
    }

    toActivities() {
        this.router.navigate(['/activities']);
    }

    toDescription() {
        this.router.navigate(['/description']);
    }
}
