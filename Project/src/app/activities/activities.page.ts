import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FileOpener} from '@ionic-native/file-opener/ngx';
import {Storage} from '@ionic/storage';
import {Events} from "@ionic/angular";

@Component({
    selector: 'app-activities',
    templateUrl: './activities.page.html',
    styleUrls: ['./activities.page.scss'],
})
export class ActivitiesPage implements OnInit {
    lastDescr: any;
    removedFiles: any;
    nowProj: any;

    constructor(private activatedRoute: ActivatedRoute,
                private fileOpener: FileOpener,
                private storage: Storage,
                private router: Router,
                private events: Events) {
        events.subscribe('upd', (time) => {
            console.log('Found updates. Updating data.\n',
                time.getDate() + "-" + time.getMonth() + "-" + time.getFullYear(), '\n' + time.getHours() + ':' + time.getMinutes());
            this.storage.ready().then(() => {
                    this.storage.get('savedDescr').then((val) => {
                        if (val != null) {
                            this.lastDescr = JSON.parse(val);
                        } else {
                            this.lastDescr = null;
                        }
                    });
                    this.storage.get('removedFiles').then((val) => {
                        if (val != null) {
                            this.removedFiles = val;
                        } else {
                            this.removedFiles = null;
                        }
                    });
                    this.storage.get('project').then((val) => {
                        if (val != null) {
                            this.nowProj = JSON.parse(val);
                        } else {
                            this.nowProj = {
                                title: 'New Project',
                                teacher: 'NaN',
                                date: undefined,
                                description: 'NaN'
                            };
                        }
                    });
                }
            );
        });
    }


    ngOnInit(): void {
        this.storage.ready().then(() => {
                this.storage.get('savedDescr').then((val) => {
                    if (val != null) {
                        this.lastDescr = JSON.parse(val);
                    } else {
                        this.lastDescr = null;
                    }
                });
                this.storage.get('removedFiles').then((val) => {
                    if (val != null) {
                        this.removedFiles = val;
                    } else {
                        this.removedFiles = null;
                    }
                });
                this.storage.get('project').then((val) => {
                    if (val != null) {
                        this.nowProj = JSON.parse(val);
                    } else {
                        this.nowProj = {
                            title: 'New Project',
                            teacher: 'NaN',
                            date: undefined,
                            description: 'NaN'
                        };
                    }
                });
            }
        );
    }

    useLastDescr() {
        const now = new Date();
        console.log(this.nowProj);
        this.storage.ready().then(() => {
            this.storage.set('project', JSON.stringify({
                title: this.lastDescr.title,
                teacher: this.lastDescr.teacher,
                date: this.lastDescr.deadline,
                description: this.lastDescr.description
            }));
            this.storage.set('savedDescr', JSON.stringify({
                date: now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear(),
                title: this.nowProj.title,
                teacher: this.nowProj.teacher,
                deadline: this.nowProj.date,
                description: this.nowProj.description
            })).then(() => this.events.publish('upd', new Date()));
        });
        /*this.lastDescr = {
            date: now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear(),
            title: nowProj.title,
            teacher: nowProj.teacher,
            deadline: nowProj.date,
            description: nowProj.description
        };*/

    }

    restore(file: any) {
        let files = [];
        this.storage.ready().then(() => {
            this.storage.get('files').then((val) => {
                if (val != null) {
                    console.log('val', val)
                    files = val;
                    files.push({
                            filePath: file.filePath,
                            fileName: file.fileName,
                            fileDescription: file.fileDescription
                        }
                    );
                    this.removedFiles.splice(this.removedFiles.indexOf(file), 1);
                    this.storage.ready().then(() => {
                        this.storage.set('files', files).catch((e) => {
                            alert(e);
                            return;
                        }).then(() =>
                            this.storage.set("removedFiles", this.removedFiles)
                                .then(() => this.events.publish('upd', new Date()))
                                .catch((e) => {
                                    alert(e);
                                    return;
                                }));
                    });
                } else files = [];
            })
        });


    }

    open(path: string) {
        const fileExtn = path.split('.').reverse()[0];
        const fileMIMEType = this.getMIMEtype(fileExtn);
        alert('ext: ' + fileExtn + ' mime: ' + fileMIMEType);
        this.fileOpener.open(path, fileMIMEType)
            .catch(e => alert('Error opening file:\n' + e));
    }

    getMIMEtype(extn) {
        const ext = extn.toLowerCase();
        const MIMETypes = {
            'txt': 'text/plain',
            'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc': 'application/msword',
            'pdf': 'application/pdf',
            'jpg': 'image/jpeg',
            'jpeg': 'image/jpeg',
            'bmp': 'image/bmp',
            'png': 'image/png',
            'xls': 'application/vnd.ms-excel',
            'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'rtf': 'application/rtf',
            'ppt': 'application/vnd.ms-powerpoint',
            'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        };
        return MIMETypes[ext];
    }

    toHome() {
        this.router.navigate(['/']);
    }

    toFiles() {
        this.router.navigate(['/files']);
    }

    toDescription() {
        this.router.navigate(['/description']);
    }
}
