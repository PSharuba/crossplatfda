import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {PipesModule} from '../pipes/pipes.module';
import {IonicModule} from '@ionic/angular';

import {DescriptionPage} from './description.page';

const routes: Routes = [
    {
        path: '',
        component: DescriptionPage
    }
];

@NgModule({
    imports: [
        PipesModule,
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [DescriptionPage]
})
export class DescriptionPageModule {
}
