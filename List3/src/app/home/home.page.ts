import {IonItemSliding} from '@ionic/angular';
import {NavController} from '@ionic/angular';
import {Component} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    items: string[] = ['Apple', 'Banana', 'Orange', 'Tomatoes'];

    constructor(public navCtrl: NavController) {

    }

    doAdd(itm: IonItemSliding) {
        const newItem: string = prompt('Insert new meal');
        this.items.push(newItem);
        itm.close();
    }

    doRemove(item: string, itm: IonItemSliding) {
        const index: number = this.items.indexOf(item);
        this.items.splice(index, 1);
        itm.close();
    }

    eat(item: string, itm: IonItemSliding) {
        alert('You eated ' + item);
        const index: number = this.items.indexOf(item);
        this.items.splice(index, 1);
        itm.close();
    }
}
