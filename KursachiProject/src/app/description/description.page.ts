import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {Events} from "@ionic/angular";
import * as $ from 'jquery';
/*
    npm install jquery --save
    npm install @types/jquery
    import * as $ from 'jquery'
*/

@Component({
    selector: 'app-description',
    templateUrl: './description.page.html',
    styleUrls: ['./description.page.scss'],
})
export class DescriptionPage implements OnInit {
    constructor(public navCtrl: NavController,
                private router: Router,
                private storage: Storage,
                private events: Events) {
        events.subscribe('upd', (time) => {
            console.log('Found updates. Updating data.\n',
                time.getDate() + "-" + time.getMonth() + "-" + time.getFullYear(), '\n' + time.getHours() + ':' + time.getMinutes());
            this.storage.ready().then(() => {
                    this.storage.get('project').then((val) => {
                        if (val != null) {
                            this.Project = JSON.parse(val);
                        } else {
                            this.Project = {
                                'title': 'New Project',
                                'teacher': 'NaN',
                                'date': undefined,
                                'description': 'NaN'
                            };
                        }
                    });
                }
            );
        });
    }

    Project: any;

    ngOnInit(): void {
        this.storage.ready().then(() => {
                this.storage.get('project').then((val) => {
                    if (val != null) {
                        this.Project = JSON.parse(val);
                    } else {
                        this.Project = {
                            title: 'New Project',
                            teacher: 'NaN',
                            date: undefined,
                            description: 'NaN'
                        };
                    }
                });
            }
        );
    }

    clear() {
        this.Project = {
            'title': 'New Project',
            'teacher': 'NaN',
            'date': undefined,
            'description': 'NaN'
        };
        this.storage.ready().then(() => {
                this.storage.set('project', JSON.stringify(this.Project))
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
                this.storage.set('files', [])
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
                this.storage.set('lastActivity', [])
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
                this.storage.remove('savedDescr')
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
                this.storage.set('removedFiles', [])
                    .catch(e => alert('Error: ' + JSON.stringify(e)));
            }
        );
        this.switchEdit();
    }

    switchEdit() {
        const div = document.getElementById('editDetails');
        $('.anim').toggleClass('start-anim');
        // @ts-ignore
        document.getElementById('title').value = this.Project.title;
        // @ts-ignore
        document.getElementById('teacher').value = this.Project.teacher;
        if (this.Project.date != undefined) {
            // @ts-ignore
            document.getElementById('date').value = this.Project.date;
        } else {
            const date = new Date();
            // @ts-ignore
            document.getElementById('date').value = date.getDate() + '-'
                + (date.getMonth() + 1) + '-'
                + date.getFullYear();
        }
        // @ts-ignore
        document.getElementById('description').value = this.Project.description;
    }

    editProject() {
        const save = this.Project;
        // @ts-ignore
        let title = document.getElementById('title').value;
        // @ts-ignore
        let teacher = document.getElementById('teacher').value;
        // @ts-ignore
        let date = document.getElementById('date').value;
        // @ts-ignore
        let descr = document.getElementById('description').value;
        if (this.Project.date === undefined && (title === '' || teacher === '' || descr === '' || date === '')) {
            alert('Invalid input!');
            return;
        }
        if (title === '') {
            title = this.Project.title;
        }
        if (teacher === '') {
            teacher = this.Project.teacher;
        }
        if (descr === '') {
            descr = this.Project.description;
        }
        if (date === '') {
            date = this.Project.date;
        }
        if (save.date !== undefined) {
            const now = new Date();
            this.storage.ready().then(() => {
                this.storage.set('savedDescr', JSON.stringify({
                    date: now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear(),
                    title: save.title,
                    teacher: save.teacher,
                    deadline: save.date,
                    description: save.description
                }));
            });
        }

        this.Project = {
            title: title,
            teacher: teacher,
            date: date,
            description: descr
        };

        this.storage.ready().then(() => {
                this.storage.set('project', JSON.stringify(this.Project)).then(() =>
                    this.events.publish('upd', new Date()));
            }
        );

        this.switchEdit();
    }

    toHome() {
        this.router.navigate(['/']);
    }

    toActivities() {
        this.router.navigate(['/activities']);
    }

    toFiles() {
        this.router.navigate(['/files']);
    }
}
