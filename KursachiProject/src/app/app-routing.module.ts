import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'description', loadChildren: './description/description.module#DescriptionPageModule' },
  { path: 'files', loadChildren: './files/files.module#FilesPageModule' },
  { path: 'activities', loadChildren: './activities/activities.module#ActivitiesPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
