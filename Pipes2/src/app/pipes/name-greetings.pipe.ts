import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'nameGreetings'
})
export class NameGreetingsPipe implements PipeTransform {
    private male = ['Pavel', 'Bob', 'Steve', 'Alex'];
    private female = ['Mary', 'Kate', 'Alexa', 'Elizabeth'];

    transform(name: string): any {
        if (this.male.includes(name)) {
            return 'Уважаемый ' + name;
        }
        if (this.female.includes(name)) {
            return 'Уважаемая ' + name;
        }
        return 'Уважаемое ' + name;
    }

}
