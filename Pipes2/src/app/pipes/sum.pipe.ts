import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'sum'
})
export class SumPipe implements PipeTransform {

    transform(x1: any, x2: any): any {
        return parseFloat(x1) + parseFloat(x2);
    }

}
