import { NgModule } from '@angular/core';
import { SumPipe } from './sum.pipe';
import { SumExtendedPipe } from './sum-extended.pipe';
import { NameGreetingsPipe } from './name-greetings.pipe';

@NgModule({
    declarations: [
        SumPipe,
        SumExtendedPipe,
        NameGreetingsPipe
    ],
    imports: [],
    exports: [
        SumPipe,
        SumExtendedPipe,
        NameGreetingsPipe
    ]
})
export class PipesModule {}
