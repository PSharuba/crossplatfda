import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'sumExtended'
})
export class SumExtendedPipe implements PipeTransform {
    private sum: number;

    transform(arr: number): any {
        this.sum = 0;

        for (; arr[0] <= arr[1]; arr[0]++) {
            this.sum += parseFloat(arr[0]);
        }
        return this.sum * arr[2];
    }

}
